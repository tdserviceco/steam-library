'use strict';
window.steamLibrary = (function() {
  /**
STEAM LIBRARY 2017
Purpose of this is to learn pure javascript.

To get your own token key: https://steamcommunity.com/dev
**/
  var errorColor = [
    'background: linear-gradient(darkred, red)',
    'border: 1px solid #3E0E02',
    'color: white',
    'display: block',
    'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)',
    'box-shadow: 0 1px 0 rgba(255, 255, 255, 0.4) inset, 0 5px 3px -5px rgba(0, 0, 0, 0.5), 0 -13px 5px -10px rgba(255, 255, 255, 0.4) inset',
    'line-height: 40px',
    'text-align: center',
    'font-weight: bold',
    'padding: 10px'
  ].join(';');
  /*-------------------------------------------------------------------------------------------------------*/

  function returnGetPlayerSummaries(steamID, key, loader) {

    //Ajax settings:
    var xhttp = new XMLHttpRequest();
    var params = "id=" + steamID + "&key=" + key;

    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        // document.getElementById(loader).innerHTML = this.responseText;
        var jsonParsed = JSON.parse(this.response);
        var image = jsonParsed.response.players[0].avatarfull;
        var profileName = jsonParsed.response.players[0].personaname;
        if(jsonParsed.response.players[0].personastate === 0) {
          var status = 'offline';
        }
        else {
          var status = 'online';
        }

        document.getElementById(loader).innerHTML = "<img src="+image+" alt='Profile image'>";
        document.getElementById(loader).innerHTML += "<h2>"+profileName+"</h2>";
        document.getElementById(loader).innerHTML += "<h6>"+status+"</h6>";
      }
    };
    xhttp.open("POST", "/js/steam-library/php_functions/getPlayerSummaries.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(params);
  }

  var loadLibrary = {
    image : null,
    profileName : null,
    status : null,
    getPlayerSummaries: function(steamID_64bits, key, loader) {
      console.log('getPlayerSummaries activated');
      if (steamID_64bits === '') {
        console.log("%c SteamID is empty", errorColor);
      } else if (key === '') {
        console.log('%c Key is empty', errorColor)
      } else {
        return returnGetPlayerSummaries(steamID_64bits, key, loader)
      }

    }
  }

  return loadLibrary;

  // END
})();
